# Elasticsearch Module for Icinga Web 2 | (c) 2018-2019 Icinga Development Team <info@icinga.com> | GPLv2+

%global revision 1
%global module_name elasticsearch

%global icingaweb_min_version 2.6.0

Name:           icingaweb2-module-%{module_name}
Version:        0.9.0
Release:        %{revision}%{?dist}
Summary:        Elasticsearch - Icinga Web 2 module
Group:          Applications/System
License:        GPLv2+
URL:            https://icinga.com
Source0:        https://github.com/Icinga/icingaweb2-module-%{module_name}/archive/v%{version}.tar.gz
#/icingaweb2-module-%{module_name}-%{version}.tar.gz
BuildArch:      noarch

%global basedir %{_datadir}/icingaweb2/modules/%{module_name}

Requires:       icingaweb2 >= %{icingaweb_min_version}
Requires:       php-Icinga >= %{icingaweb_min_version}

%description
The Elasticsearch Module for Icinga Web 2 integrates your Elastic stack into
Icinga Web 2. Based on Elasticsearch instances and event types you configure,
the module allows you to display data collected by Beats, Logstash and any
other source. After you've installed and configured the module, you can browse
events via the host action Elasticsearch Events.

It also brings a command for icingacliwhich can be used to query Elasticsearch
for certain events. This command can be used to create Icinga 2 checks.

%prep
%setup -q
#-n icingaweb2-module-%{module_name}-%{version}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{basedir}

cp -r * %{buildroot}%{basedir}

%clean
rm -rf %{buildroot}

%preun
set -e

# Only for removal
if [ $1 == 0 ]; then
    echo "Disabling icingaweb2 module '%{module_name}'"
    rm -f /etc/icingaweb2/enabledModules/%{module_name}
fi

exit 0

%files
%doc README.md COPYING

%defattr(-,root,root)
%{basedir}

%changelog
* Mon Sep 09 2019 Markus Frosch <markus.frosch@icinga.com> - 0.9.0-1
- Release version 0.9.0-1

* Thu Aug 29 2019 Markus Frosch <markus.frosch@icinga.com> - 0.9.0-1
- Initial package version
